package com.example.demo.services;

import java.security.InvalidKeyException;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domain.Atividade;
import com.example.demo.repositories.AtividadeRepository;

import javassist.NotFoundException;

@Service
public class AtividadeService {

	@Autowired
	public AtividadeRepository atividadeRepository;
	
	public List<Atividade> findByNomeContainingIgnoreCase(String nome) {
		return atividadeRepository.findByNomeContainingIgnoreCase(nome);
	}
	
	public Atividade getOne(Integer id) throws NotFoundException {
		return atividadeRepository.findById(id).orElseThrow(() -> new NotFoundException("Registro não encontrado"));
	}
	
	public Atividade create(Atividade atividade) {
		return atividadeRepository.save(atividade);
	}
	
	public void update(Atividade atividade) throws InvalidKeyException, NotFoundException {
		if(atividade.getId() == null) {
			throw new InvalidKeyException("Id não pode ser nulo.");
		}
		
		getOne(atividade.getId());
		
		atividadeRepository.save(atividade);
	}
	
	public void delete(Integer id) throws NotFoundException {
		getOne(id);
		
		atividadeRepository.deleteById(id);
	}
	
}
