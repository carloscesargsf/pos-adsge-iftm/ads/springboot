package com.example.demo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.domain.Atividade;

@Repository
public interface AtividadeRepository extends JpaRepository<Atividade, Integer> {

	List<Atividade> findByNomeContainingIgnoreCase(String nome);
	
	Optional<Atividade> findById(Integer id);
	
}
